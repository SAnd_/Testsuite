<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd"
    xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>1D Oscillator</title>
    <authors>
      <author>Ben Heinrich</author>
    </authors>
    <date>2022-11-30</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>eigenValue</keyword>
    </keywords>
    <references></references>
    <isVerified>yes</isVerified>
    <description>
    A single degree of freedom oscillator composed of a square mass (1mx1m) with density 1 kg/m3 in 2d (plane strain).
    We constrain in x direction and connect a concentrated stiffness in the y direction.
    
    The resulting system has a mass M=1, k=4*pi^2, thus, the natural frequency is 1 Hz.

    This is a testcase for the eigenvalue step solving a standard EVP, it is equivalent to the EigenValueDriver_Standard 
    testcase for ARPACK.
    The resulting eigenvalues and eigenvectors are imported from a matrix market file in array format.
    In addition, the eigenvalues and eigenvectors are printed in the terminal by the python script to test logging.
    </description>  
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="EigenValueStandardArray.h5ref"/>
      <!--gmsh fileName="UnitSquare.msh"/-->
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType='plane'  printGridInfo="no">
    <regionList>
      <region name="V_square" material="UnitMaterial" />
    </regionList>
  </domain>
  
  <sequenceStep index="1">
    <analysis>
      <eigenValue>
        <valuesAround>
          <shiftPoint>
	    <Real>0</Real>
	    <Imag>-5</Imag>
          </shiftPoint>
          <number>3</number>
        </valuesAround>
        <eigenVectors normalization="norm" side="right"/>
        <problemType>
          <Standard>
            <Matrix>stiffness</Matrix>
          </Standard>
        </problemType>
      </eigenValue>
    </analysis>
      
    <pdeList>
      <mechanic subType='planeStrain'>
        <regionList>
          <region name="V_square"/>
        </regionList>        
        <bcsAndLoads>
          <fix name="S_fixed">
            <comp dof="x"/>
          </fix>
          <concentratedElem name="N_spring" dof="y" stiffnessValue="4*pi*pi"/>
        </bcsAndLoads>      
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <eigenSolver id="ext"/>
          </standard>
        </solutionStrategy>
        <eigenSolverList>
          <external id="ext">
            <logging>yes</logging>
            <cmd>python3 EigenSolver.py</cmd>
            <arguments>
	      <AFileName/>
              <shiftPoint/>
	      <number/>
              <arg>SI</arg>
              <tolerance>10e-9</tolerance>
              <EigenValuesFileName/>
              <EigenVectorsFileName/>
	    </arguments>
          </external>
        </eigenSolverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
</cfsSimulation>
