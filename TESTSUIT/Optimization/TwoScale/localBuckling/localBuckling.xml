<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" id="0">
  <documentation>
    <title>test for interpolation of local buckling load factor</title>
    <authors>
      <author>Daniel Huebner</author>
    </authors>
    <date>2023-12-06</date>
    <keywords>
      <keyword>buckling</keyword>
    </keywords>
    <references>doi:10.1007/s00158-023-03619-4</references>
    <isVerified>no</isVerified>
    <description>
      mesh created with 'create_mesh.py --res 5 --width 1 --type bulk2d --file globalBuckling.mesh'
    </description>
  </documentation>
  <fileFormats>
    <input>
      <hdf5 fileName="localBuckling.h5ref"/>
    </input>
    <output>
      <hdf5 />
    </output>
    <materialData file="./mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="strong" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static />
    </analysis>
    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech"/>
        </regionList>
        <bcsAndLoads>
          <fix name="left">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <force name="bottom_right">
            <comp dof="y" value="-1"/>
          </force>
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechStress">
            <allRegions />
          </elemResult>
          <nodeResult type="mechRhsLoad">
            <allRegions />
          </nodeResult>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <optimization log="">
    <costFunction type="slack" task="maximize" linear="false" >
      <stopping queue="5" value="1e-3" type="designChange"/>
    </costFunction>

    <constraint type="localBucklingLoadFactor" bound="lowerBound" value="slack" mode="constraint"/>
    
    <constraint type="compliance" bound="equal" value="-1" sequence="1" mode="observation"/>

    <constraint type="globalTwoScaleVolume" bound="upperBound" value="0.5" linear="true" mode="constraint">
      <local normalize="true" power="1" />
    </constraint>
    
    <optimizer type="snopt" maxIterations="5">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option name="verify_level" type="integer" value="-1"/>
      </snopt>
    </optimizer>
      
    <ersatzMaterial material="mechanic" method="paramMat" region="mech">
      <filters>
        <filter neighborhood="maxEdge" value="1.6" type="density" design="stiff1"/>
      </filters>
      <paramMat>
        <designMaterials>
          <designMaterial type="hom-rect-C1" sequence="1">
            <param name="stiff2" value="0.0"/>
            <param name="rotAngle" value="0.0"/>
            <param name="shear1" value="0.5"/>
            <homRectC1 file="catalogue_cubit_diamond_b0.600_0.001000_r3.xml"/>
          </designMaterial>
        </designMaterials>
      </paramMat>
      <design name="stiff1" initial=".4" lower="0.1" upper="1.0" region="mech"/>
      <design name="slack" initial="1e-8" lower="0" upper="1e10"/>
      <result value="design" design="stiff1" id="optResult_1" access="smart"/>
      <result value="localBucklingLoadFactor" id="optResult_2"/>
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
  
</cfsSimulation>
