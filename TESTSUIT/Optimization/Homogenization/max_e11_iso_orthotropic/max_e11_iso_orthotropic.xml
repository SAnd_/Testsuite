<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>max E11 with iso-orthotropic constraints</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2015-05-12</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>The easiest inverse homogenization. Maximize E11 with iso-orthotropic constraints. Minimal density filter.
    It is essential to use a no-uniform initial design for homogenization! 
    Use create_density.py to generate initial designs and show_density.py to visualize the result.
    </description>
  </documentation>


  <fileFormats>
    <output>
      <hdf5 />
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="99lines"/>
    </regionList>
    <nodeList>
      <nodes name="center">
        <coord x="0.5" y="0.5"/>
      </nodes>
    </nodeList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
           <fix name="center"> 
              <comp dof="x"/> <comp dof="y"/> 
           </fix>
          
           <periodic slave="north" master="south" dof="x" quantity="mechDisplacement"/>
           <periodic slave="north" master="south" dof="y" quantity="mechDisplacement"/>
           <periodic master="west"  slave="east" dof="x"  quantity="mechDisplacement" />
           <periodic master="west"  slave="east" dof="y"  quantity="mechDisplacement" />
          
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>          
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>             
       </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <loadErsatzMaterial file="circular_2d-v_0.5-inv_10.density.xml" set="last"/>

  <optimization>
    <costFunction type="homTensor" coord="11" task="maximize" multiple_excitation="true">
      <multipleExcitation type="homogenizationTestStrains" />
    </costFunction>

    <constraint type="iso-orthotropy" mode="constraint"/>
    <constraint type="volume" value="0.5" mode="constraint" bound="upperBound" />
    <constraint type="volume" mode="observation" access="physical" />
    <constraint type="greyness" mode="observation"/>
    <constraint type="greyness" mode="observation" access="physical"  />
    
    <optimizer type="scpip" maxIterations="50">
      <snopt>
        <option name="verify_level" type="integer" value="0"/>
      </snopt>
    </optimizer>

    <ersatzMaterial method="simp" material="mechanic">
      <regions>
        <region name="mech"/>
      </regions>
      <filters>
        <filter neighborhood="maxEdge" value="1.1" type="density"/>
      </filters>
      <!-- the external initial design has too low density, therefore enforce_bounds to prevent an assert while filtering -->
      <design name="density" initial="0.5" lower="1e-2" upper="1" enforce_bounds="true" region="mech" />
      <transferFunction type="simp" application="mech" design="density" param="5"/>
      <export/>
    </ersatzMaterial>
  </optimization>  
</cfsSimulation>


