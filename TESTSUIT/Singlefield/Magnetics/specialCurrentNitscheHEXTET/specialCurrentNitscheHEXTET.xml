<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
    
    <documentation>
        <title>Two bricks connected via NC Nitsche interface</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2019-06-25</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description> The two bricks are meshed with HEX-TET </description>
    </documentation>
    
    <fileFormats>
        <input>
            <cdb fileName="mesh.cdb"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain geometryType="3d">
        <regionList>
            <region name="V_low" material="copper"/> <!--iron-->
            <region name="V_up" material="air"/>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_3"/>
            <surfRegion name="S_4"/>
            <surfRegion name="S_5"/>
            <surfRegion name="S_6"/>
            <surfRegion name="S_9"/>
            <surfRegion name="S_10"/>
            <surfRegion name="S_11"/>
            <surfRegion name="S_12"/>
            <surfRegion name="NC_up"/>
            <surfRegion name="NC_low"/>
        </surfRegionList>
        <ncInterfaceList>
            <ncInterface name="zP" masterSide="NC_up" slaveSide="NC_low"/>
        </ncInterfaceList>
    </domain>
    
    <fePolynomialList>
        <Legendre id="default">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Legendre id="low">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>



    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <elecConduction>
                <regionList>
                    <region name="V_low" polyId="H1"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="S_4" value="1"/>
                    <potential name="S_6" value="0"/>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <regionResult type="elecGradVInt">
                        <allRegions/>
                    </regionResult>
                </storeResults>
            </elecConduction>
        </pdeList>
    </sequenceStep>
    




    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        
    <pdeList>
        <magneticEdge>
            <regionList>
                <region name="V_up" polyId="default"/>
                <region name="V_low" polyId="low"/>
            </regionList>
            
           <ncInterfaceList>
               <ncInterface name="zP" formulation="Nitsche" nitscheFactor="1000"/>
           </ncInterfaceList>
            
        
            <bcsAndLoads>               
                <!--surfaces of V_low-->
                <fluxParallel name="S_3"/>
                <fluxParallel name="S_4"/>
                <fluxParallel name="S_5"/>
                <fluxParallel name="S_6"/>
                
                <fluxParallel name="S_top"/>
                
                <!--surfaces of V_up-->
                <fluxParallel name="S_9"/>
                <fluxParallel name="S_10"/>
                <fluxParallel name="S_11"/>
                <fluxParallel name="S_12"/>
                
            </bcsAndLoads>
            
            <coilList>
                <coil id="coil1">
                    <source type="current" value="1e4"/>
                    <part id="1">
                        <regionList>
                            <region name="V_low"/>
                        </regionList>
                        <direction>
                            <analytic>
                                <comp dof="x" value="1"/>
                            </analytic>
                        </direction>
                        <wireCrossSection area="1"/>
                    </part>
                </coil>
            </coilList>
            
            <storeResults>
                <elemResult type="magFluxDensity">
                    <allRegions/>
                </elemResult>
                <elemResult type="magEddyCurrentDensity">
                    <allRegions/>
                </elemResult>
                <elemResult type="magTotalCurrentDensity">
                    <allRegions/>
                </elemResult>
                <elemResult type="magPotential">
                    <allRegions/>
                </elemResult>
                <regionResult type="magEnergy">
                    <regionList>
                        <region name="V_up" outputIds="txt"/>
                        <region name="V_low" outputIds="txt"/>
                    </regionList>
                </regionResult>
                <regionResult type="magJouleLossPower">
                    <regionList>
                        <region name="V_up" outputIds="txt"/>
                        <region name="V_low" outputIds="txt"/>
                    </regionList>
                </regionResult>
            </storeResults>
        </magneticEdge>            
    </pdeList>

        <linearSystems>
            <system>
                <solverList>
                    <pardiso></pardiso>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
    
    <sequenceStep index="3">
        <analysis>
            <harmonic>
                <numFreq>3</numFreq>
                <startFreq>10</startFreq>
                <stopFreq>30</stopFreq>
            </harmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V_up" polyId="default"/>
                    <region name="V_low" polyId="low"/>
                </regionList>
                
                <ncInterfaceList>
                    <ncInterface name="zP" formulation="Nitsche" nitscheFactor="1000"/>
                </ncInterfaceList>
                
                
                <bcsAndLoads>               
                    <!--surfaces of V_low-->
                    <fluxParallel name="S_3"/>
                    <fluxParallel name="S_4"/>
                    <fluxParallel name="S_5"/>
                    <fluxParallel name="S_6"/>
                    
                    <fluxParallel name="S_top"/>
                    
                    <!--surfaces of V_up-->
                    <fluxParallel name="S_9"/>
                    <fluxParallel name="S_10"/>
                    <fluxParallel name="S_11"/>
                    <fluxParallel name="S_12"/>
                    
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil1">
                        <source type="current" value="1e4"/>
                        <part id="1">
                            <regionList>
                                <region name="V_low"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="x" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="1"/>
                        </part>
                    </coil>
                </coilList>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <regionResult type="magEnergy">
                        <regionList>
                            <region name="V_up" outputIds="txt"/>
                            <region name="V_low" outputIds="txt"/>
                        </regionList>
                    </regionResult>
                    <regionResult type="magJouleLossPower">
                        <regionList>
                            <region name="V_up" outputIds="txt"/>
                            <region name="V_low" outputIds="txt"/>
                        </regionList>
                    </regionResult>
                </storeResults>
            </magneticEdge>            
        </pdeList>
        
        <linearSystems>
            <system>
                <solverList>
                    <pardiso></pardiso>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
    
    <sequenceStep index="4">
        <analysis>
            <transient>
                <numSteps>10</numSteps>
                <deltaT>0.01</deltaT>
            </transient>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V_up" polyId="default"/>
                    <region name="V_low" polyId="low"/>
                </regionList>
                
                <ncInterfaceList>
                    <ncInterface name="zP" formulation="Nitsche" nitscheFactor="1000"/>
                </ncInterfaceList>
                
                
                <bcsAndLoads>               
                    <!--surfaces of V_low-->
                    <fluxParallel name="S_3"/>
                    <fluxParallel name="S_4"/>
                    <fluxParallel name="S_5"/>
                    <fluxParallel name="S_6"/>
                    
                    <fluxParallel name="S_top"/>
                    
                    <!--surfaces of V_up-->
                    <fluxParallel name="S_9"/>
                    <fluxParallel name="S_10"/>
                    <fluxParallel name="S_11"/>
                    <fluxParallel name="S_12"/>
                    
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil1">
                        <source type="current" value="1e4*sin(10*pi*2*t)"/>
                        <part id="1">
                            <regionList>
                                <region name="V_low"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="x" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="1"/>
                        </part>
                    </coil>
                </coilList>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <regionResult type="magEnergy">
                        <regionList>
                            <region name="V_up" outputIds="txt"/>
                            <region name="V_low" outputIds="txt"/>
                        </regionList>
                    </regionResult>
                    <regionResult type="magJouleLossPower">
                        <regionList>
                            <region name="V_up" outputIds="txt"/>
                            <region name="V_low" outputIds="txt"/>
                        </regionList>
                    </regionResult>
                </storeResults>
            </magneticEdge>            
        </pdeList>
        
        <linearSystems>
            <system>
                <solverList>
                    <pardiso></pardiso>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
    
</cfsSimulation>
