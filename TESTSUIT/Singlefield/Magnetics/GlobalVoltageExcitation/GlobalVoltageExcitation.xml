<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.cfs++.org/simulation">
    <!-- define which files are needed for simulation input & output-->
    
    
    <documentation>
        <title>Straight Massive Voltage Excitation</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2019-06-18</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description> We condiser a straight massive wire (inductor) in air </description>
    </documentation>
    <fileFormats>
        <input>
            <cdb fileName="meshCylAir.cdb"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain geometryType="3d">
        <regionList>
            <region name="V_air" material="air"/>
            <region name="V_coil" material="copper_unitConduc_4_elecConduction"/>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_air_out"/>
            <surfRegion name="S_z_pos"/>
            <surfRegion name="S_z_neg"/>
            <surfRegion name="S_sigma_plus"/>
            <surfRegion name="S_sigma_minus"/>
        </surfRegionList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>


    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <elecConduction>
                <regionList>
                    <region name="V_coil" polyId="H1"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="S_sigma_plus" value="1"/>
                    <potential name="S_sigma_minus" value="0"/>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <regionResult type="elecGradVInt">
                        <allRegions/>
                    </regionResult> 
                </storeResults>
            </elecConduction>
        </pdeList>
    </sequenceStep>


    <!--
    =========================
    Without reordering
    ==============================
    -->
    <sequenceStep index="2">
        <analysis>
            <harmonic>
                <numFreq>5</numFreq>
                <startFreq>10</startFreq>
                <stopFreq>1000</stopFreq>
            </harmonic>
        </analysis>

        <pdeList>
            <magneticEdge formulation="specialA-V">
                <regionList>
                    <region name="V_coil" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                </regionList>

                <bcsAndLoads>
                    <fluxParallel name="S_air_out"/>
                    <fluxParallel name="S_z_pos"/>
                    <fluxParallel name="S_z_neg"/>
                    <fluxParallel name="S_sigma_plus"/>
                    <fluxParallel name="S_sigma_minus"/>
                    
                </bcsAndLoads>

                <coilList>
                    <coil id="coil1">
                        <source type="specialcurrent" value="10"/>
                        <part id="1">
                            <regionList>
                                <region name="V_coil"/>
                            </regionList>
                            <direction>
                                <external normalise="no">
                                    <sequenceStep index="1">
                                        <quantity name="elecCurrentDensity" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </direction>
                            <gradV_in_gradV>
                                <external>
                                    <sequenceStep>
                                        <quantity name="elecGradVInt" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </gradV_in_gradV>
                            <wireCrossSection area="1"/> <!-- has to be unit area! -->
                            <resistance value="0"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                 <!--   <surfRegionResult type="magEddyCurrent" complexFormat="amplPhase">
                        <surfRegionList>
                            <surfRegion name="S_sigma_plus"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <coilResult type="coilCurrent">
                        <coilList>
                            <coil id="coil1" outputIds="txt"/>
                        </coilList>
                    </coilResult>
                    <coilResult type="coilVoltage" complexFormat="realImag">
                        <coilList>
                            <coil id="coil1"/>
                        </coilList>
                    </coilResult>-->
                </storeResults>
            </magneticEdge>
        </pdeList>


        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <matrix reordering="noReordering"/>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
    
    <!--
    =========================
    With reordering
    ==============================
    -->
    <sequenceStep index="3">
        <analysis>
            <harmonic>
                <numFreq>5</numFreq>
                <startFreq>10</startFreq>
                <stopFreq>1000</stopFreq>
            </harmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge formulation="specialA-V">
                <regionList>
                    <region name="V_coil" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                </regionList>
                
                <bcsAndLoads>
                    <fluxParallel name="S_air_out"/>
                    <fluxParallel name="S_z_pos"/>
                    <fluxParallel name="S_z_neg"/>
                    <fluxParallel name="S_sigma_plus"/>
                    <fluxParallel name="S_sigma_minus"/>
                    
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil1">
                        <source type="specialcurrent" value="10"/>
                        <part id="1">
                            <regionList>
                                <region name="V_coil"/>
                            </regionList>
                            <direction>
                                <external normalise="no">
                                    <sequenceStep index="1">
                                        <quantity name="elecCurrentDensity" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </direction>
                            <gradV_in_gradV>
                                <external>
                                    <sequenceStep>
                                        <quantity name="elecGradVInt" pdeName="elecConduction"/>
                                        <timeFreqMapping>
                                            <constant/>
                                        </timeFreqMapping>
                                    </sequenceStep>
                                </external>
                            </gradV_in_gradV>
                            <wireCrossSection area="1"/> <!-- has to be unit area! -->
                            <resistance value="0"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                  <!--  <surfRegionResult type="magEddyCurrent" complexFormat="amplPhase">
                        <surfRegionList>
                            <surfRegion name="S_sigma_plus"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <coilResult type="coilCurrent">
                        <coilList>
                            <coil id="coil1" outputIds="txt"/>
                        </coilList>
                    </coilResult>
                    <coilResult type="coilVoltage" complexFormat="realImag">
                        <coilList>
                            <coil id="coil1"/>
                        </coilList>
                    </coilResult> -->
                </storeResults>
            </magneticEdge>
        </pdeList>
        
        
        <linearSystems>
            <system>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    
</cfsSimulation>
