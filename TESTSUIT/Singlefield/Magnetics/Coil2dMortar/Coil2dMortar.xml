<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Mortar 2D Coil</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2009-04-14</date>
    <keywords>
      <keyword>coil</keyword>
      <keyword>lagrange multiplier</keyword>
      <keyword>magneticNodal</keyword>
      <keyword>magneticScalar</keyword>
      <keyword>mortar fem</keyword>
      <keyword>nonmatching grids</keyword>
      <keyword>static</keyword>
    </keywords>
    <references>none</references>
    <isVerified>yes</isVerified>
    <description>
      This simulation describes a simple 2D coil modelled using nonmatching
      grids. Both subdomains share the same material properties, so that the
      field may also enter the outer (air) region which only consists of a
      one-element layer. 
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="coil2d_slave.h5" id="slave"
        readEntities="coil coil_neumann slave_iface1 slave_iface2"
        linearizeEntities="__all__"/>
      <hdf5 fileName="coil2d_master.h5" id="master"
        readEntities="air air_neumann master_iface1 master_iface2 dirichlet"
        linearizeEntities="__none__"/>
    </input>

    <output>
      <hdf5 id="hdf5"/>
    </output>

    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="air" material="Iron"/>
      <region name="coil" material="Iron"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="coil_neumann"/>
      <surfRegion name="slave_iface1"/>
      <surfRegion name="slave_iface2"/>
      <surfRegion name="air_neumann"/>
      <surfRegion name="master_iface1"/>
      <surfRegion name="master_iface2"/>
      <surfRegion name="dirichlet"/>      
    </surfRegionList>
    
    <ncInterfaceList>
      <ncInterface name="nc_iface1" masterSide="master_iface1" slaveSide="slave_iface1"/>
      <ncInterface name="nc_iface2" masterSide="master_iface2" slaveSide="slave_iface2"/>
    </ncInterfaceList>
  </domain>

  <fePolynomialList>
    <Lagrange>
      <gridOrder/>
    </Lagrange>
  </fePolynomialList>
  
  <integrationSchemeList>
    <scheme>
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
  </integrationSchemeList>
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <magnetic>
        <regionList>
          <region name="air"/>
          <region name="coil"/>
        </regionList>

        <ncInterfaceList>
          <ncInterface name="nc_iface1" formulation="Mortar"/>
          <ncInterface name="nc_iface2" formulation="Mortar"/>
        </ncInterfaceList>
        
        <bcsAndLoads>
          <fluxParallel name="dirichlet">
            <comp dof="-"/>
          </fluxParallel>
        </bcsAndLoads>
        
	<coilList>
           <coil id="myCoil">
            <source type="current" value="1.0"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
		<direction>
		  <analytic/>
		</direction>
                <wireCrossSection area="1"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
         
        
        <storeResults>
          <nodeResult type="magPotential">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </magnetic>
     
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <!--<setup idbcHandling="penalty"/>-->
            <setup idbcHandling="elimination"/>
            <exportLinSys baseName="fespace"/>
            <matrix storage="sparseNonSym" reordering="noReordering"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct>no</symStruct>
            <IterRefineSteps>100</IterRefineSteps>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

</cfsSimulation>
