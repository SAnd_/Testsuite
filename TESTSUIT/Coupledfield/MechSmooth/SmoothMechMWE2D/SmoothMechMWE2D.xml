<?xml version="1.0"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.cfs++.org file:/home/domini/Devel/CFS_SRC/CFS/share/xml/CFS-Simulation/CFS.xsd"
  xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>transient coupling of two domains (smooth-mech)</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2021-11-25</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
   	  Transient simulation of two iteratively coupled domains. We excite the domain of the mechanic PDE and apply the Dirichlet BC to the domain of the smooth PDE, which is fixed on the other side. The displacement can be calcualted via linear interpolation. This example demonstrates the forward coupling with a special PDE order (smooth-mechanic). In this case exactly three iterations are necessary. With the number of iterations the iterative coupling can also be checked.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <!--<cdb fileName="SmoothMech_MWE.cdb"/>-->
      <hdf5 fileName="SmoothMechMWE2D.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Air_1" material="FluidMat"/>
      <region name="Air_2" material="FluidMat"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="Exc_1"/>
      <surfRegion name="Fix_1_t"/>
      <surfRegion name="Fix_1_b"/>
      <surfRegion name="IF"/>
      <surfRegion name="Fix_2_t"/>
      <surfRegion name="Fix_2_b"/>
      <surfRegion name="Exc_2"/>
    </surfRegionList>
  </domain>

  <sequenceStep index="1"> 
    <analysis>
      <transient>
        <numSteps>3</numSteps>
        <deltaT>2e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <mechanic subType="planeStrain" timeStepAlpha="-0.25">
        <regionList>
          <region name="Air_1"/>
        </regionList>

        <bcsAndLoads>
          <fix name="Fix_1_t">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_1_b">
            <comp dof="y"/>
          </fix>
          <displacement name="Exc_1">
            <comp dof="x" value="10e-6*sin(2*pi*1000*t)*(1-exp(-(t/1e-3)^1))"/>
          </displacement>
          <pressure name="IF">
            <coupling pdeName="smooth">
              <quantity name="smoothZeroStress"/>
            </coupling>
          </pressure>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="mechVelocity">
            <allRegions/>
          </nodeResult>
          <nodeResult type="mechAcceleration">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
      
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_2"/>
        </regionList>
        
        <bcsAndLoads>
          <fix name="Fix_2_t">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_2_b">
            <comp dof="y"/>
          </fix>
          <displacement name="IF">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement"/>
            </coupling>
          </displacement>
          <fix name="Exc_2">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothZeroStress">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>   
      
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="smooth;mechanic">
        <convergence logging="yes" maxNumIters="5" stopOnDivergence="no">
          <quantity name="mechDisplacement" value="1e-3" normType="rel"/>
          <quantity name="mechVelocity" value="1e-3" normType="rel"/>
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Air_1"/>
          <region name="Air_2"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
