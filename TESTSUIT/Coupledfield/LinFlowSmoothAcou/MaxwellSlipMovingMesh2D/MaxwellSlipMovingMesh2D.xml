<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>MaxwellSlipMovingMesh2D</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2022-07-28</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> Non Just funcionality test </references>
    <isVerified>no</isVerified>
    <description>
      With this test the implementation of the MaxwellSlipBC based on the vectorial Lagrange multiplier for the LinFlow PDE on moving domains gets tested. This test consists of a channel which is excited with two timely seperated velocity pulses. In between those pulses the domain changes leading to different stress gradients at the boundaries and therefore different slip velocities.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <cdb fileName="Channel.cdb"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="w_1" value="2e-3" />
      <var name="w_2" value="4e-3" />
      <var name="amp" value="0.5*w_1/2" />
      <var name="dt1" value="2.5e-5" />
      <var name="steps_1" value="10" />
      <var name="steps_trans" value="10" />
      <var name="steps_pause" value="10" />
    </variableList>

    <regionList>
      <region name="Channel_1" material="FluidMat"/>
      <region name="Chamber" material="FluidMat"/>
      <region name="Channel_2" material="FluidMat"/>
      <region name="Channel_acou" material="FluidMat"/>
      <region name="PML" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Exc"/>
      <surfRegion name="NoSlip"/>
      <surfRegion name="IFM"/>
      <surfRegion name="Sym"/>
      <surfRegion name="BC_Maxwell_X"/>
      <surfRegion name="BC_Maxwell_Y"/>
      <surfRegion name="BC_Fix"/>
      <surfRegion name="BC_X"/>
      <surfRegion name="IFS"/>
    </surfRegionList> 

    <ncInterfaceList>
      <ncInterface name="NCI" masterSide="IFM" slaveSide="IFS"></ncInterface>
    </ncInterfaceList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <Lagrange id="orderLag">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integLag">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>40</numSteps>
        <deltaT>dt1</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Chamber"/>
        </regionList>

        <bcsAndLoads>
          <fix name="BC_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Maxwell_X">
            <comp dof="x" value="(t lt steps_1*dt1)? (0) : ((t lt (steps_1+steps_trans)*dt1)? (amp*x/(w_1/2)*1/2*(1-cos(pi*(t-dt1*steps_1)/(dt1*steps_trans)))) : (amp*x/(w_1/2)))"/>
            <comp dof="y" value="0"/>
          </displacement>
          <displacement name="BC_X">
            <comp dof="x" value="(t lt steps_1*dt1)? (0) : ((t lt (steps_1+steps_trans)*dt1)? (amp*(w_2/2-x)/(w_1/2)*1/2*(1-cos(pi*(t-dt1*steps_1)/(dt1*steps_trans)))) : (amp*(w_2/2-x)/(w_1/2)))"/>
            <comp dof="y" value="0" />
          </displacement>
          <displacement name="BC_Maxwell_Y">
            <comp dof="x" value="(t lt steps_1*dt1)? (0) : ((t lt (steps_1+steps_trans)*dt1)? (amp*1/2*(1-cos(pi*(t-dt1*steps_1)/(dt1*steps_trans)))) : (amp))"/>
            <comp dof="y" value="0"/>
          </displacement>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </smooth>

      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Chamber" movingMeshId="moveGridID"/>
          <region name="Channel_1"/>
          <region name="Channel_2"/>
        </regionList>

        <movingMeshList>
          <movingMesh name="moveGridID">
            <coupling pdeName="smooth">
              <quantity name="smoothVelocity" />
            </coupling>
          </movingMesh>
        </movingMeshList>
        
        <bcsAndLoads> 
          <noSlip name="NoSlip">
            <comp dof="x"/>   
            <comp dof="y"/>      
          </noSlip>

          <noSlip name="Sym">
            <comp dof="x"/>        
          </noSlip>

          <velocity name="Exc">
            <comp dof="x" value="0"/>
            <comp dof="y" value="(t lt steps_1*dt1)? (sin(pi*t/(steps_1*dt1))*cos(pi/2*x/1e-3)) : ((t gt (steps_1+steps_trans+steps_pause)*dt1)? (sin(pi*(t-(steps_1+steps_trans+steps_pause)*dt1)/(steps_1*dt1))*cos(pi/2*x/1e-3)) : (0))"/>
          </velocity>

          <velocityConstraint name="BC_Maxwell_X" volumeRegion="Chamber">
            <MaxwellFirstOrderSlip meanFreePath="68e-9" C1="1000" />
          </velocityConstraint>

          <velocityConstraint name="BC_Maxwell_Y" volumeRegion="Chamber">
            <MaxwellFirstOrderSlip meanFreePath="68e-9" C1="1000" />
          </velocityConstraint>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="lagrangeMultiplier">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="lagrangeMultiplier1">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </fluidMechLin>

      <acoustic formulation="acouPotential">
        <regionList>
          <region name="Channel_acou" polyId="orderPres"/>
          <region name="PML" dampingId="myPML" polyId="orderPres"/>
        </regionList>

        <dampingList>
          <pml id="myPML">
            <type>inverseDist</type>
            <dampFactor>1.0</dampFactor>
          </pml>
        </dampingList>
        
        <storeResults>
          <nodeResult type="acouPotential">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <elemResult type="acouVelocity">
            <allRegions outputIds="h5"/>
          </elemResult>
        </storeResults>
      </acoustic>
    </pdeList>

    <couplingList>
      <direct>
        <linFlowAcouDirect>
          <ncInterfaceList>
            <ncInterface name="NCI"/>
          </ncInterfaceList>
        </linFlowAcouDirect>
      </direct>
      <iterative PDEorder="mechanic;smooth">
        <convergence logging="yes" maxNumIters="5" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechPressure" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Chamber"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct>no</symStruct>
            <logging>yes</logging>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
